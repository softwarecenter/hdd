<?php
namespace common\models;

use Yii;
use yii\base\Exception;
use yii\base\Model;

/**
 * Login form
 */
class HddForm extends Model
{

    public $model;
    public $sn;
    public $pn;
    public $fw;
    public $photo;
    public $comment;
    public $address;
    public $token;

    /** @var  UploadedFile */
    public $imageFile;
    public $image;


    /** @var  User */
    public $_user;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['model', 'sn', 'pn', 'fw', 'comment', 'address', 'token'], 'required'],
            [['model', 'sn', 'pn', 'fw', 'photo', 'comment', 'address', 'token'], 'string'],
            ['image', 'file'],
            ['token', 'validateToken'],
            ['imageFile', 'safe'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateToken($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user) {
                $this->addError($attribute, 'Incorrect token.');
            }
        }
    }


    /**
     * @return bool
     */
    public function upload()
    {
        if (!$this->imageFile) {
            return false;
        }

        $this->photo = 'uploads/' . md5($this->imageFile->baseName + time()) . '.' . $this->imageFile->extension;

        if ($this->validate()) {
            return $this->imageFile->saveAs($this->photo);
        } else {
            return false;
        }
    }


    public function save()
    {
        if ($this->validate()) {

            if ($this->imageFile && !$this->upload()) {
                throw new Exception('Upload file error.');
            }

            $model = new Hdd();
            $model->model = $this->model;
            $model->sn = $this->sn;
            $model->pn = $this->pn;
            $model->fw = $this->fw;
            $model->photo = $this->photo;
            $model->comment = $this->comment;
            $model->address = $this->address;
            $model->created_time = date('Y-m-d H:i:s');

            return $model->save();
        }
        
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByToken($this->token);
        }

        return $this->_user;
    }
}
