<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hdd".
 *
 * @property int $id
 * @property string $created_time
 * @property string $model
 * @property string $sn
 * @property string $pn
 * @property string $fw
 * @property string $photo
 * @property string $comment
 * @property string $address
 */
class Hdd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hdd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_time'], 'safe'],
            [['model', 'sn', 'pn', 'fw', 'photo', 'comment', 'address'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_time' => 'Created Time',
            'model' => 'Model',
            'sn' => 'Sn',
            'pn' => 'Pn',
            'fw' => 'Fw',
            'photo' => 'Photo',
            'comment' => 'Comment',
            'address' => 'Address',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\HddQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\HddQuery(get_called_class());
    }
}
