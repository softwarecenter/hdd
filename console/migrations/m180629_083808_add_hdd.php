<?php

use yii\db\Migration;

/**
 * Class m180629_083808_add_hdd
 */
class m180629_083808_add_hdd extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('hdd',
            [
                'id' => $this->primaryKey(),
                'created_time' => $this->dateTime(),
                'model' => $this->string(255),
                'sn' => $this->string(255),
                'pn' => $this->string(255),
                'fw' => $this->string(255),
                'photo' => $this->string(255),
                'comment' => $this->string(255),
                'address' => $this->string(255),
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('hdd');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180629_083808_add_hdd cannot be reverted.\n";

        return false;
    }
    */
}
