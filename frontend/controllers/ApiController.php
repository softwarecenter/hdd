<?php

namespace frontend\controllers;

use common\components\exceptions\ApiException;
use common\helpers\Date;
use common\models\HddForm;
use common\models\LoginForm;
use Yii;
use yii\base\Exception;
use yii\base\InvalidRouteException;
use yii\base\Module;
use yii\base\UserException;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\UploadedFile;

class ApiController extends Controller
{

    const CODE_SUCCESS = 0;
    const CODE_RUNTIME_ERROR = 10;
    const CODE_BAD_REQUEST = 400;
    const CODE_NOT_FOUND = 404;

    private $_code = 0;
    private $_result = [];
    private $_error = [];

    protected $runtimeErrorName = "_system";

    /**
     * @var string the name of the error when the exception name cannot be determined.
     * Defaults to "Error".
     */
    public $defaultName;
    /**
     * @var string the message to be displayed when the exception message contains sensitive information.
     * Defaults to "An internal server error occurred.".
     */
    public $defaultMessage;

    public function behaviors()
    {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    'Origin' => ['*'],
                ],

            ],
        ];
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }

    /**
     * @param string $id
     * @param array $params
     * @return string
     * @throws InvalidRouteException
     */
    public function runAction($id, $params = [])
    {
        $action = $this->createAction($id);
        if ($action === null) {
            throw new InvalidRouteException('Unable to resolve the request: ' . $this->getUniqueId() . '/' . $id);
        }

        Yii::trace('Route to run: ' . $action->getUniqueId(), __METHOD__);

        if (Yii::$app->requestedAction === null) {
            Yii::$app->requestedAction = $action;
        }

        $oldAction = $this->action;
        $this->action = $action;

        $modules = [];
        $runAction = true;

        // call beforeAction on modules
        foreach ($this->getModules() as $module) {
            if ($module->beforeAction($action)) {
                array_unshift($modules, $module);
            } else {
                $runAction = false;
                break;
            }
        }

        $result = null;

        if ($runAction && $this->beforeAction($action)) {
            // run the action
            try {
                $result = $action->runWithParams($params);

                $result = $this->afterAction($action, $result);

                // call afterAction on modules
                foreach ($modules as $module) {
                    /* @var $module Module */
                    $result = $module->afterAction($action, $result);
                }
                if (is_object($result)) {
                    $c = explode("\\", get_class($result));
                    $class = array_pop($c);
                    $this->_result[$class] = [];
                    foreach ($result->exportData() as $attr) {
                        $this->_result[$class][$attr] = $result->$attr;
                    }

                } else {
                    $this->_result = $result;
                }

            } catch (Exception $e) {
                $code = $e->getCode() ?: self::CODE_RUNTIME_ERROR;
                $error = $e->getError();

                if ($code == 400) {
                    $fp = @fopen("log_err.txt", "a");
                    @fwrite($fp, "\r\n\r\n" . Date::now() . "\r\nACTION " . Yii::$app->request->getUrl() . "\r\nDATA " . Yii::$app->request->getRawBody() . "\r\n");
                    @fclose($fp);
                }

                if (!is_array($error)) {
                    $error = [$this->runtimeErrorName => [0 => $error]];
                }
                $errorMessageAr = [];
                foreach ($error as $k => $ar) {
                    if (is_array($ar) && isset($ar[0])) {
                        $errorMessageAr[$k] = $ar[0];
                    } else {
                        $errorMessageAr[$k] = $ar;
                    }
                }
                $error["_summary"] = implode(" ", $errorMessageAr);
                $this->_result = [];
                $this->_error = $error;
                $this->_code = $code;
                Yii::$app->response->statusCode = 200;
            }
        }

        $this->action = $oldAction;
        Yii::$app->response->getHeaders()->set('Content-Type', 'text/json');
        return Json::encode(["code" => $this->_code, "result" => $this->_result, "error" => $this->_error]);
    }

    /**
     * @return array
     * @throws Exception
     */
    public function actionAuth()
    {
        $model = new LoginForm();
        $model->setAttributes(\Yii::$app->request->post());

        if ($model->validate()) {
            $model->_user->auth_token = sha1(time() . $model->username);
            if ($model->_user->save()) {
                return ['token' => $model->_user->auth_token];
            }
            throw new ApiException($model->_user->getErrors());
        } else {
            throw new ApiException($model->getErrors());
        }
    }

    /**
     * @return array
     * @throws Exception
     */
    public function actionHdd()
    {
        $model = new HddForm();

        $model->setAttributes(\Yii::$app->request->post());

        $model->imageFile = UploadedFile::getInstanceByName('image');

        if ($model->save()) {

            return ['status' => 'success'];

        } else {
            throw new ApiException($model->getErrors());
        }
    }
}