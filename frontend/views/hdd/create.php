<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Hdd */

$this->title = 'Create Hdd';
$this->params['breadcrumbs'][] = ['label' => 'Hdds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hdd-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
