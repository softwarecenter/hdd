<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\HddSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hdd-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'created_time') ?>

    <?= $form->field($model, 'model') ?>

    <?= $form->field($model, 'sn') ?>

    <?= $form->field($model, 'pn') ?>

    <?php // echo $form->field($model, 'fw') ?>

    <?php // echo $form->field($model, 'photo') ?>

    <?php // echo $form->field($model, 'comment') ?>

    <?php // echo $form->field($model, 'address') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
